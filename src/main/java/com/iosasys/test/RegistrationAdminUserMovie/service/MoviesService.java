package com.iosasys.test.RegistrationAdminUserMovie.service;

import com.iosasys.test.RegistrationAdminUserMovie.dto.MoviesDTO;
import com.iosasys.test.RegistrationAdminUserMovie.model.*;
import com.iosasys.test.RegistrationAdminUserMovie.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@Service
public class MoviesService {

    private final AdminRepository adminRepository;
    private final MovieActorRepository movieActorRepository;
    private final MovieDirectorRepository movieDirectorRepository;
    private final MovieGenreRepository movieGenreRepository;
    private final MoviesRepository moviesRepository;
    private final ActorsRepository actorsRepository;
    private final DirectorRepository directorRepository;
    private final GenreRepository genreRepository;

    public MoviesService(AdminRepository adminRepository,
                         MovieActorRepository movieActorRepository,
                         MovieDirectorRepository movieDirectorRepository,
                         MovieGenreRepository movieGenreRepository,
                         MoviesRepository moviesRepository,
                         ActorsRepository actorsRepository,
                         DirectorRepository directorRepository,
                         GenreRepository genreRepository) {
        this.adminRepository = adminRepository;
        this.movieActorRepository = movieActorRepository;
        this.movieDirectorRepository = movieDirectorRepository;
        this.movieGenreRepository = movieGenreRepository;
        this.moviesRepository = moviesRepository;
        this.actorsRepository = actorsRepository;
        this.directorRepository = directorRepository;
        this.genreRepository = genreRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public String createNewMovie(long id, MoviesDTO moviesDTO, Map<String, Integer> score) {

        adminRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Esse usuário não é administrador"));

        var movieName = new Movies(moviesDTO.getName());
        var movieNameSaved = moviesRepository.save(movieName);

        for (String actor : moviesDTO.getActor()) {

            var foundActor = actorsRepository.findByName(actor);

            long actorId;

            if (foundActor.isEmpty()) {
                var r = createNewActor(actor);
                actorId = r.getId();
            } else actorId = foundActor.get().getId();

            var movieActorEntity = new MovieActor(movieNameSaved.getId(), actorId);
            movieActorRepository.save(movieActorEntity);
        }

        for (String genre : moviesDTO.getGenre()) {

            var foundGenre = genreRepository.findGenreByName(genre);

            long genreId;

            if (foundGenre.isEmpty()) {
                var r = createNewGenre(genre);
                genreId = r.getId();
            } else genreId = foundGenre.get().getId();

            var movieGenreEntity = new MovieGenre(moviesDTO.getId(), genreId);
            movieGenreRepository.save(movieGenreEntity);
        }

        for (String director : moviesDTO.getDirector()) {

            var foundDirector = directorRepository.findDirectorByName(director);

            long directorId;

            if (foundDirector.isEmpty()) {
                var r = createNewGenre(director);
                directorId = r.getId();
            } else directorId = foundDirector.get().getId();

            var movieDirectorEntity = new MovieDirector(moviesDTO.getId(), directorId);
            movieDirectorRepository.save(movieDirectorEntity);
        }

        return null;
    }

//    Opção de filtros por diretor
    @Transactional(rollbackFor = Exception.class)
    public List<Movies> getMoviesByDirector(long directorId) {

        var result = movieDirectorRepository
                .findAllDirectorsById(directorId)
                .orElseThrow(
                        () -> new NoSuchElementException("Não há filmes com esse diretor"));

        var listaDeFilmes = new ArrayList<Movies>();

        for (Integer i : result) {
            var movie = moviesRepository
                    .findById(i.longValue())
                    .orElseThrow(() -> new NoSuchElementException("Não há filme com o id " + i));
            listaDeFilmes.add(movie);
        }

        return listaDeFilmes;
    }

    //    Opção de filtros por nome, gênero e/ou atores
    @Transactional(rollbackFor = Exception.class)
    public Movies getMoviesByName(String name) {

        return moviesRepository
                .findByName(name)
                .orElseThrow(
                        () -> new NoSuchElementException("O filme " + name + " não existe em nosso banco de dados"));
    }

    //    Opção de filtros por gênero e/ou atores
    @Transactional(rollbackFor = Exception.class)
    public List<Movies> getMoviesByGenre(String genre) {

        var queryGenre = genreRepository
                .findGenreByName(genre)
                .orElseThrow(() -> new NoSuchElementException("O gênero " + genre + " não existe em nosso banco de dados"));

        var movieIdList = movieGenreRepository
                .findAllMoviesByGenre(queryGenre.getId())
                .orElseThrow(() -> new NoSuchElementException("Não há filmes do gênero " + queryGenre.getName() + " cadastrados."));

        var listaDeFilmes = new ArrayList<Movies>();

        for (MovieGenre i : movieIdList) {

//            var movie = moviesRepository.
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Actor createNewActor(String name) {
        var actor = new Actor(name);
        return actorsRepository.save(actor);
    }

    @Transactional(rollbackFor = Exception.class)
    public Director createNewDirector(String name) {
        var director = new Director(name);
        return directorRepository.save(director);
    }

    @Transactional(rollbackFor = Exception.class)
    public Genre createNewGenre(String name) {
        var genre = new Genre(name);
        return genreRepository.save(genre);
    }
}
