package com.iosasys.test.RegistrationAdminUserMovie.service;

import com.iosasys.test.RegistrationAdminUserMovie.dto.AdminDTO;
import com.iosasys.test.RegistrationAdminUserMovie.exception.NotFoundException;
import com.iosasys.test.RegistrationAdminUserMovie.model.Admin;
import com.iosasys.test.RegistrationAdminUserMovie.model.User;
import com.iosasys.test.RegistrationAdminUserMovie.repository.AdminRepository;
import com.iosasys.test.RegistrationAdminUserMovie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class AdminService {

    private final AdminRepository adminRepository;
    private final UserRepository userRepository;

    @Autowired
    public AdminService(AdminRepository repository, UserRepository userRepository) {
        this.adminRepository = repository;
        this.userRepository = userRepository;
    }

    @Transactional
    public List<Admin> findAllAdmin() {

        return adminRepository.findAll();
    }

    @Transactional
    public Admin findAdminById(long id) {

        return adminRepository
                .findById(id)
                .orElseThrow(
                        () -> new NoSuchElementException("Não há nenhum administrador com o id " + id));
    }

    @Transactional
    public List<User> findAllNonAdminActive() throws NotFoundException {

        return userRepository
                .findAllActiveUsers()
                .orElseThrow(
                        () -> new NotFoundException("Não há usuários inativos"));
    }

    @Transactional(rollbackFor = Exception.class)
    public Admin createNewAdmin(@Valid AdminDTO adminDTO) {

        Admin admin = new Admin(adminDTO.getUsername(),
                adminDTO.getFirst_name(),
                adminDTO.getLast_name(),
                adminDTO.getCpf(),
                adminDTO.getPassword());

        return adminRepository.save(admin);
    }

    @Transactional(rollbackFor = Exception.class)
    public Admin updateAdmin(Long id, AdminDTO adminDTO) {

        var query = adminRepository
                .findById(id)
                .orElseThrow(
                        () -> new NoSuchElementException("Não existe nenhum administrador com o id " + id));

        query.setFirst_name(adminDTO.getFirst_name());
        query.setLast_name(adminDTO.getLast_name());
        query.setUsername(adminDTO.getUsername());

        return adminRepository.save(query);
    }

    @Transactional(rollbackFor = Exception.class)
    public Admin setAdminAsDisabled(long id) throws ExpressionException {

        var query = adminRepository
                .findById(id)
                .orElseThrow(
                        () -> new NoSuchElementException("Não existe um administrador com id " + id));

        if (Boolean.FALSE.equals(query.getIs_enabled()))
            throw new ExpressionException("O administrador " + query.getUsername() + " está inativo.");

        query.setIs_enabled(false);

        return adminRepository.save(query);
    }
}
