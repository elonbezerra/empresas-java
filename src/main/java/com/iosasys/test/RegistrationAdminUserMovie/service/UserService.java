package com.iosasys.test.RegistrationAdminUserMovie.service;

import com.iosasys.test.RegistrationAdminUserMovie.model.MovieScore;
import com.iosasys.test.RegistrationAdminUserMovie.dto.UserDTO;
import com.iosasys.test.RegistrationAdminUserMovie.model.User;
import com.iosasys.test.RegistrationAdminUserMovie.repository.MovieScoreRepository;
import com.iosasys.test.RegistrationAdminUserMovie.repository.MoviesRepository;
import com.iosasys.test.RegistrationAdminUserMovie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.Map;
import java.util.NoSuchElementException;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final MoviesRepository moviesRepository;
    private final MovieScoreRepository movieScoreRepository;

    @Autowired
    public UserService(UserRepository userRepository,
                       MoviesRepository moviesRepository,
                       MovieScoreRepository movieScoreRepository) {
        this.userRepository = userRepository;
        this.moviesRepository = moviesRepository;
        this.movieScoreRepository = movieScoreRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public User createNewUser(@Valid UserDTO userDTO) {
        var newUser = new User(
                userDTO.getUsername(),
                userDTO.getFirst_name(),
                userDTO.getLast_name(),
                userDTO.getCpf(),
                userDTO.getPassword());

        return userRepository.save(newUser);
    }

    @Transactional(rollbackFor = Exception.class)
    public User updateUser(long id, @Valid UserDTO userDTO) {

        var user = userRepository
                .findById(id)
                .orElseThrow(
                        () -> new NoSuchElementException("O usuário de id " + id + "não foi encontrado"));

        user.setUsername(userDTO.getUsername());
        user.setFirst_name(userDTO.getFirst_name());
        user.setLast_name(userDTO.getLast_name());

        return userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public void setUserStatusToDisabled(long id) {
        var user = userRepository
                .findById(id)
                .orElseThrow(
                        () -> new NoSuchElementException("O usuário de id " + id + "não foi encontrado"));

        user.setIs_enabled(false);

        userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean userVote(long id,
                            UserDTO userDTO,
                            Map<String, Integer> score) {

        var movie = moviesRepository
                .findById(id)
                .orElseThrow(
                        () -> new NoSuchElementException("Filme não encontrado"));

        var vote = score.values().stream().findFirst().orElseThrow(
                () -> new NoSuchElementException("É necessário atribuir uma nota para votar"));

        if (vote > 4 || vote < 0) throw new ArithmeticException("Você só pode usar números de 0 a 4");

        var movieScore = new MovieScore(movie.getId(), vote);

        movieScoreRepository.save(movieScore);
        return true;
    }
}
