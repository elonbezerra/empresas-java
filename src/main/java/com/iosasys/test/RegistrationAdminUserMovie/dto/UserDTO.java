package com.iosasys.test.RegistrationAdminUserMovie.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserDTO {

    private long id;

    @NotEmpty
    @NotNull
    private String username;

    @NotEmpty
    @NotNull
    private String first_name;

    @NotEmpty
    @NotNull
    private String last_name;

    @NotEmpty
    @NotNull
    @CPF(message = "cpf incorreto")
    @Length(max = 11, min = 11)
    private String cpf;

    @NotEmpty
    @NotNull
    private String password;

    public UserDTO() {
    }

    public UserDTO(String username,
                   String first_name,
                   String last_name,
                   String cpf,
                   String password) {
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.cpf = cpf;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public String getCpf() {
        return cpf;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
