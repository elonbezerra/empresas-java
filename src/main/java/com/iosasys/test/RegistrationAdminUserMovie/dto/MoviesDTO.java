package com.iosasys.test.RegistrationAdminUserMovie.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class MoviesDTO {

    private long id;

    @NotNull
    @NotEmpty
    private List<String> director;

    @NotEmpty
    @NotNull
    private String name;

    @NotEmpty
    @NotNull
    private List<String> genre;

    @NotEmpty
    @NotNull
    private List<String> actor;

    private int score;

    public MoviesDTO() {
    }

    public MoviesDTO(long id,
                     List<String> director,
                     String name,
                     List<String> genre,
                     List<String> actor, int score) {
        this.id = id;
        this.director = director;
        this.name = name;
        this.genre = genre;
        this.actor = actor;
        this.score = score;
    }

    public MoviesDTO(List<String> director,
                     String name,
                     @NotEmpty @NotNull List<String> genre,
                     @NotEmpty @NotNull List<String> actor) {
        this.director = director;
        this.name = name;
        this.genre = genre;
        this.actor = actor;
    }

    public long getId() {
        return id;
    }

    public List<String> getDirector() {
        return director;
    }

    public void setDirector(List<String> director) {
        this.director = director;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public List<String> getActor() {
        return actor;
    }

    public void setActor(List<String> actor) {
        this.actor = actor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
