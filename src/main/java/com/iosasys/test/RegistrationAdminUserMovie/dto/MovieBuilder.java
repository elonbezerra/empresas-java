package com.iosasys.test.RegistrationAdminUserMovie.dto;

import java.util.List;

public class MovieBuilder {

    private long id;
    private String name;
    private List<String> actors;
    private List<String> directors;
    private List<String> genres;
    private int score;

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public void setDirectors(List<String> directors) {
        this.directors = directors;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public MoviesDTO build() {
        return new MoviesDTO(id, directors, name, genres, actors, score);
    }
}
