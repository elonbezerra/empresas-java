package com.iosasys.test.RegistrationAdminUserMovie.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_MOVIE_USER_SCORE")
public class MovieScore {

    @Id
    private long id;

    @Column
    private long id_movie;

    @Column
    private int score;

    public MovieScore() {
    }

    public MovieScore(long id_movie,
                      int score) {
        this.id_movie = id_movie;
        this.score = score;
    }

    public long getId() {
        return id;
    }

    public long getId_movie() {
        return id_movie;
    }

    public void setId_movie(long id_movie) {
        this.id_movie = id_movie;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
