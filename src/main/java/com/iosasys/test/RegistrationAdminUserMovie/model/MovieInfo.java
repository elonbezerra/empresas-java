package com.iosasys.test.RegistrationAdminUserMovie.model;

import java.util.List;

public class MovieInfo {

    private long id;
    private List<String> actors;
    private List<Director> directors;
    private List<Genre> genres;
    private int score;

    public MovieInfo(long id,
                     List<String> actors,
                     List<Director> directors,
                     List<Genre> genres,
                     int score) {
        this.id = id;
        this.actors = actors;
        this.directors = directors;
        this.genres = genres;
        this.score = score;
    }
}
