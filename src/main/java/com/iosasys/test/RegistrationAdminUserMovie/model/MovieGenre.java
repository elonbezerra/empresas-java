package com.iosasys.test.RegistrationAdminUserMovie.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_MOVIE_GENRE")
public class MovieGenre {

    @Id
    private long id;

    @Column
    private long id_movie;

    @Column
    private long id_genre;

    public MovieGenre() {
    }

    public MovieGenre(long id_movie, long id_genre) {
        this.id_movie = id_movie;
        this.id_genre = id_genre;
    }

    public long getId() {
        return id;
    }

    public long getId_movie() {
        return id_movie;
    }

    public void setId_movie(long id_movie) {
        this.id_movie = id_movie;
    }

    public long getId_genre() {
        return id_genre;
    }

    public void setId_genre(long id_genre) {
        this.id_genre = id_genre;
    }
}
