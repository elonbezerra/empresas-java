package com.iosasys.test.RegistrationAdminUserMovie.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "TB_MOVIE_DIRECTOR")
public class MovieDirector {

    @Id
    private long id;

    @Column
    private long id_movie;

    @Column
    private long id_director;

    public MovieDirector() {
    }

    public MovieDirector(long id_movie, long id_director) {
        this.id_movie = id_movie;
        this.id_director = id_director;
    }

    public long getId() {
        return id;
    }

    public long getId_movie() {
        return id_movie;
    }

    public void setId_movie(long id_movie) {
        this.id_movie = id_movie;
    }

    public long getId_director() {
        return id_director;
    }

    public void setId_director(long id_director) {
        this.id_director = id_director;
    }
}
