package com.iosasys.test.RegistrationAdminUserMovie.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_MOVIE_ACTOR")
public class MovieActor {

    @Id
    private long id;

    @Column
    private long id_movie;

    @Column
    private long id_actor;

    public MovieActor() {
    }

    public MovieActor(long id_movie, long id_actor) {
        this.id_movie = id_movie;
        this.id_actor = id_actor;
    }

    public long getId() {
        return id;
    }

    public long getId_movie() {
        return id_movie;
    }

    public void setId_movie(long id_movie) {
        this.id_movie = id_movie;
    }

    public long getId_actor() {
        return id_actor;
    }

    public void setId_actor(long id_actor) {
        this.id_actor = id_actor;
    }
}
