package com.iosasys.test.RegistrationAdminUserMovie.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.*;

@Entity
@Table(name = "TB_USER")
public class User {

    @Id
    private long id;

    @Column
    private String username;

    @Column
    private String first_name;

    @Column
    private String last_name;

    @Column
    private String cpf;

    @Column
    private String password;

    @Column
    private Boolean is_enabled;

    @Column
    private String role;

    public User() {
    }

    public User(String username,
                String first_name,
                String last_name,
                String cpf,
                String password
    ) {
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.cpf = cpf;
        this.password = password;
        this.is_enabled = true;
        this.role = "USER";
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getIs_enabled() {
        return is_enabled;
    }

    public void setIs_enabled(Boolean is_enabled) {
        this.is_enabled = is_enabled;
    }

    public String getRole() {
        return role;
    }

    public String getCpf() {
        return cpf;
    }
}
