package com.iosasys.test.RegistrationAdminUserMovie.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_ACTORS")
public class Actor {

    @Id
    private long id;

    @Column
    private String name;

    public Actor(String name) {
        this.name = name;
    }

    public Actor() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
