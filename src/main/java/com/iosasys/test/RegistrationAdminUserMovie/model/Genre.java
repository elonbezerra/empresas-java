package com.iosasys.test.RegistrationAdminUserMovie.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_GENRE")
public class Genre {

    @Id
    private long id;

    @Column
    private String name;

    public Genre(String name) {
        this.name = name;
    }

    public Genre() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
