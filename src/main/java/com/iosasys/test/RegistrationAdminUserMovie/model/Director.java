package com.iosasys.test.RegistrationAdminUserMovie.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_DIRECTOR")
public class Director {

    @Id
    private long id;

    @Column
    private String name;

    public Director(String name) {
        this.name = name;
    }

    public Director() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
