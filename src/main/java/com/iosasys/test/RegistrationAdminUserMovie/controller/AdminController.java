package com.iosasys.test.RegistrationAdminUserMovie.controller;

import com.iosasys.test.RegistrationAdminUserMovie.dto.AdminDTO;
import com.iosasys.test.RegistrationAdminUserMovie.exception.NotFoundException;
import com.iosasys.test.RegistrationAdminUserMovie.model.Admin;
import com.iosasys.test.RegistrationAdminUserMovie.model.User;
import com.iosasys.test.RegistrationAdminUserMovie.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "api/v1/admin")
@Api(value = "API REST Admin")
@CrossOrigin(origins = "*")
public class AdminController {

    private final AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping
    @ApiOperation(value = "Método responsável por retornar todos os administradores registrados")
    public ResponseEntity<List<Admin>> getAllAdmin() {

        var response = adminService.findAllAdmin();

        return new ResponseEntity<>(response, OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Método responsável por retornar administrador por id")
    public ResponseEntity<Admin> getAdminById(@PathVariable long id) {

        var response = adminService.findAdminById(id);

        return new ResponseEntity<>(response, OK);
    }

    @GetMapping("/non-admin-active")
    @ApiOperation(value = "Método responsável por retornar todos os não administradores ativos")
    public ResponseEntity<List<User>> getAllNonAdminActive() throws NotFoundException {

        var response = adminService.findAllNonAdminActive();

        return new ResponseEntity<>(response, OK);
    }

    @PostMapping
    @ApiOperation(value = "Método responsável por registrar um administrador")
    public ResponseEntity<Admin> registryAdmin(@RequestBody AdminDTO adminDTO) {

        var response = adminService.createNewAdmin(adminDTO);

        return new ResponseEntity<>(response, CREATED);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Método responsável por atualizar informações do administrador")
    public ResponseEntity<Admin> modifyAdmin(@PathVariable long id, @RequestBody AdminDTO adminDTO) {

        var response = adminService.updateAdmin(id, adminDTO);

        return new ResponseEntity<>(response, OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Método responsável por desativar o administrador")
    public ResponseEntity<Admin> deleteAdmin(@PathVariable long id) {

        var response = adminService.setAdminAsDisabled(id);

        return new ResponseEntity<>(response, OK);
    }
}
