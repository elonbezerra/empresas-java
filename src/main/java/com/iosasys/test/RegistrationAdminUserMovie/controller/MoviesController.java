package com.iosasys.test.RegistrationAdminUserMovie.controller;

import com.iosasys.test.RegistrationAdminUserMovie.dto.MoviesDTO;
import com.iosasys.test.RegistrationAdminUserMovie.dto.UserDTO;
import com.iosasys.test.RegistrationAdminUserMovie.service.MoviesService;
import com.iosasys.test.RegistrationAdminUserMovie.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "api/v1/movies")
@Api(value = "API REST Movies")
@CrossOrigin(origins = "*")
public class MoviesController {

    private final MoviesService moviesService;
    private final UserService userService;

    public MoviesController(MoviesService moviesService, UserService userService) {
        this.moviesService = moviesService;
        this.userService = userService;
    }

    @PostMapping
    @ApiOperation(value = "Este método permite que o administrador salve um novo filme")
    public ResponseEntity<?> postNewMovie(@RequestBody MoviesDTO moviesDTO,
                                          @RequestHeader Map<String, Integer> score) {
        // id de autenticação
        long id = 1;

        var result = moviesService.createNewMovie(id, moviesDTO, score);

        return new ResponseEntity<>(CREATED);
    }

    @PostMapping("/{id}")
    @ApiOperation(value = "Este método permite que o usuário vote em um filme")
    public ResponseEntity<?> vote(@PathVariable long id,
                                  @RequestBody UserDTO userDTO,
                                  @RequestHeader Map<String, Integer> score) {
        var result = userService.userVote(id, userDTO, score);

        return new ResponseEntity<>(CREATED);
    }
}
