package com.iosasys.test.RegistrationAdminUserMovie.controller;

import com.iosasys.test.RegistrationAdminUserMovie.dto.UserDTO;
import com.iosasys.test.RegistrationAdminUserMovie.model.User;
import com.iosasys.test.RegistrationAdminUserMovie.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "api/v1/user")
@Api(value = "API REST User")
@CrossOrigin(origins = "*")
public class UserController {
    private final UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping
    @ApiOperation(value = "Este método permite criar um novo usuário")
    public ResponseEntity<User> registryNewUser(@RequestBody UserDTO userDTO) {
        var response = service.createNewUser(userDTO);

        return new ResponseEntity<>(response, CREATED);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Este método permite atualizar as informações de um usuário")
    public ResponseEntity<User> modifyUser(@RequestBody UserDTO userDTO, @PathVariable long id) {
        var response = service.updateUser(id, userDTO);

        return new ResponseEntity<>(response, OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Este método permite desativar um usuário")
    public ResponseEntity<Void> disableUser(@PathVariable long id) {

        service.setUserStatusToDisabled(id);

        return new ResponseEntity<>(OK);
    }
}
