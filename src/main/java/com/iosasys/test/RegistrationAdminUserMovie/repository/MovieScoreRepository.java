package com.iosasys.test.RegistrationAdminUserMovie.repository;

import com.iosasys.test.RegistrationAdminUserMovie.model.MovieScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieScoreRepository extends JpaRepository<MovieScore, Long> {
}
