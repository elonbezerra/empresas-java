package com.iosasys.test.RegistrationAdminUserMovie.repository;

import com.iosasys.test.RegistrationAdminUserMovie.model.MovieActor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieActorRepository extends JpaRepository<MovieActor, Long> {
}
