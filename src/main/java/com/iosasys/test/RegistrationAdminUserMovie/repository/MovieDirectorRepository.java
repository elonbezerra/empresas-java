package com.iosasys.test.RegistrationAdminUserMovie.repository;

import com.iosasys.test.RegistrationAdminUserMovie.model.MovieDirector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovieDirectorRepository extends JpaRepository<MovieDirector, Long> {

    @Query(value = "select * from tb_movie_director md where id_director = ?1", nativeQuery = true)
    Optional<List<Integer>> findAllDirectorsById(long id);
}
