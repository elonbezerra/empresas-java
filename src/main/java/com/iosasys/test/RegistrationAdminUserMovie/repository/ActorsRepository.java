package com.iosasys.test.RegistrationAdminUserMovie.repository;

import com.iosasys.test.RegistrationAdminUserMovie.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActorsRepository extends JpaRepository<Actor, Long> {

    @Query(value = "select * from tb_actors a where name = ?1", nativeQuery = true)
    Optional<Actor> findByName(String name);
}
