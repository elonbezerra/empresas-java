package com.iosasys.test.RegistrationAdminUserMovie.repository;

import com.iosasys.test.RegistrationAdminUserMovie.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Long> {
    @Query(value = "select * from tb_directors d where name = ?1", nativeQuery = true)
    Optional<Director> findDirectorByName(String name);

    @Query(value = "select * from tb_directors d where id = ?1", nativeQuery = true)
    Optional<Director> findDirectorById(long id);
}
