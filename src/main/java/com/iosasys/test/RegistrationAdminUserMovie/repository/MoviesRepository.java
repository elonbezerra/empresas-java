package com.iosasys.test.RegistrationAdminUserMovie.repository;

import com.iosasys.test.RegistrationAdminUserMovie.model.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MoviesRepository extends JpaRepository<Movies, Long> {

    @Query(value = "select * from tb_movies m where name = ?1", nativeQuery = true)
    Optional<Movies> findByName(String name);

    @Query(value = "select * from tb_movie_genre where id_movie = :IdMovie and id_genre = :IdGenre ", nativeQuery = true)
    Optional<Movies> findMovieByDirectorAndName(@Param("IdMovie") long id_movie,@Param("IdGenre") long id_genre);
}
