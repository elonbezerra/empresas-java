create table tb_adm
(
    id         bigint auto_increment
        primary key,
    username   varchar(250)                not null,
    first_name varchar(200)                not null,
    last_name  varchar(200)                not null,
    cpf        varchar(11)                 not null,
    password   text                        not null,
    is_enabled tinyint(1)                  not null,
    role       varchar(10) default 'ADMIN' not null
);


create table tb_user
(
    id         bigint auto_increment      not null
        primary key,
    username   varchar(250)               not null,
    first_name varchar(250)               not null,
    last_name  varchar(250)               not null,
    cpf        varchar(11)                not null,
    password   text                       not null,
    is_enabled tinyint(1)                 not null,
    role       varchar(10) default 'USER' not null
);


create table tb_movies
(
    id   bigint       not null
        primary key,
    name varchar(250) not null
);


create table tb_genre
(
    id         bigint auto_increment
        primary key,
    genre_name varchar(250) null
);


create table tb_directors
(
    id   bigint auto_increment
        primary key,
    name varchar(255) not null
);


create table tb_actors
(
    id   bigint auto_increment
        primary key,
    name varchar(255) not null
);


create table tb_movie_actor
(
    id       bigint auto_increment
        primary key,
    id_movie bigint not null,
    id_actor bigint not null,
    constraint THIS_TARGET_ACTOR_fk
        foreign key (id_actor) references tb_actors (id),
    constraint THIS_TARGET_MOVIE__fk
        foreign key (id_movie) references tb_movies (id)
);


create table tb_movie_director
(
    id          bigint auto_increment
        primary key,
    id_movie    bigint not null,
    id_director bigint not null,
    constraint TB_DIRECTOR_ID_fk
        foreign key (id_director) references tb_directors (id),
    constraint TB_MOVIE_ID_fk
        foreign key (id_movie) references tb_movies (id)
);


create table tb_movie_genre
(
    id       bigint not null
        primary key,
    id_movie bigint not null,
    id_genre bigint null,
    constraint THIS_TARGET_GENRE_fk
        foreign key (id_genre) references tb_genre (ID),
    constraint THIS_TARGET_MOVIE_fk
        foreign key (id_movie) references tb_movies (id)
);


create table tb_movie_user_score
(
    id          bigint auto_increment
        primary key,
    id_user     bigint not null,
    id_movie    bigint not null,
    score_given int    not null,
    constraint THIS_TB_MOVIE_fk
        foreign key (id_movie) references tb_movies (id),
    constraint THIS_TB_USER_fk
        foreign key (id_user) references tb_user (id)
);

